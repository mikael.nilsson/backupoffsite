#!/bin/bash

ssh <NAS-SSH-USER>@home -p <PORT-IN-FIREWALL> -D 3000 -fNM

cd backup # Put all backups in this folder offsite
TSOCKS_CONF_FILE=$HOME/tsocks.conf tsocks lftp -u <NAS-FTP-USER>,"<NAS-FTP-PASSWORD>" ftp://<NAS-LAN-ADDRESS> << EOF
cd <WHEREVER-YOUR-BACKUPS-ARE-IN-YOUR-FILESYSTEM>
# copy only newer, 1000 simultaneously, remove NAS-deleted
mirror --only-newer --parallel=1000 -e

bye
EOF
