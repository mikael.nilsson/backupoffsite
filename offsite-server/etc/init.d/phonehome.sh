#!/bin/bash

LOGFILE=/var/log/phonehome.log

echo "$(date): calling home" >> $LOGFILE

ssh -NT -R <LOCAL-PORT>:localhost:22 -p <PORT-IN-FIREWALL> <NAS-SSH-USER>@home >> $LOGFILE 2>> $LOGFILE