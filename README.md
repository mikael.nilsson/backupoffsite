# Backup offsite

Wanted to keep backups offsite. Had a Raspberry Pi that I hadn't figured out a use for, perfect! I went cheap on storage, bought a MicroSD of 500GB, but there's no reason other than money to go wild and buy a 22TB disk and put in a USB enclosure, sexy!

# Architecture

I have a local backup-server (A zyxel NAS) that keeps backups locally, and my raspberry that connects to my NAS periodically and copies the latest backups via ftp.

Upon boot the offsite server runs a script that opens a ssh connection to my NAS. This tunnel I can then use to connect via my NAS back to the offsite server for admin.

My offsite server uses this tunnel to get the backup files via ftp.

# The files

- phonehome.service defines the service on the offsite server that runs phonehome.sh on boot

- phonehome.sh connects to my nas

- get_file.sh connects by ftp and syncs my backups from the NAS to the offsite server

- wpa_supplicant.conf specifies WIFI connections. I have one config for my phone hotspot and one for my home network, and upon placing the offsite server offsite, as the offsite server starts it connects to my phones hotspot letting me create a config file for the wifi there.

- tsocks.conf is the socks proxy config used by my ftp script

# Setup

- Forward a port to your NAS in your router/firewall, easiest to forward it to 22 so you don't have to change that in your NAS.
- Copy all the files to their respective places on the NAS and the offsite server
- Create ssh keypairs according to [this guide](https://serverpilot.io/docs/how-to-use-ssh-public-key-authentication/). I created keypairs both on my NAS and my raspberry, makes it easy to connect from NAS to offsite for managing.
- Exchange public keys between NAS and offsite server
- Replace all values in <> (<USER>, <PORT> etc) with values you favour. Make sure you use your forwarded port in phonehome.sh and get_files.sh. You don't HAVE to differ the NAS-SSH-USER from the NAS-FTP-USER, but my NAS is freaky and didn't let me use one account for both

  - for <URL-OF-HOME> I used a no ip solution like dyndns or afraid.org
  - I setup ssh config for my NAS on my offsite-server both via internet and LAN to simplify the config process
  - My NAS is oo-old so I needed to add HostKeyAlgorithms, try to remove that line from .ssh/config. If it works, keep it off
  - for /etc/wpa_supplicant/wpa_supplicant.conf, run
    ```sh
    wpa_passphrase <SSID>
    ```
    and input pass phrase to get the neccessary config. I have a config for my home LAN and my phone hotspot where hotspot has highest prio. This makes it easy to test and when I boot it offsite it will connect to hotspot letting me add the WIFI config there. Keeping the hotspot highest prio will let me connect for troubleshooting offsite.

- Not sure if totally neccessary, but phonehome.sh logs to /var/log/phonehome.log, you might need to
  ```sh
  sudo touch /var/log/phonehome.log
  ```
- Make an initial copy of your backups at home, so you don't have to do it offsite
- <TODO> schedule copying script something something cronjob something something

# Deploy

- At the wherever where you offsite-server is to live, turn on your hotspot on your phone
- boot the offsite-server
- from termux on your phone, ssh to your offsite-server. I used net analyzer on my phone to find the IP of my server
- do the wpa_supplicant-magic by
  ```sh
  wpa_passphrase <SSID>
  ```
  entering the passphrase and copying the result into /etc/wpa_supplicant/wpa_supplicant.conf
- reboot your offsite-server, be sure to disable your hotspot
- If everything works correctly the offsite-server should connect to the offsite LAN and connect to your NAS. As you have a port forwarded to your NAS you should be able to ssh your phone -> your NAS -> offsite server and see that the connection works

# Troubleshooting

There is soo many ways this can fk, there is a few ways you can check whats failing

- Can you ssh to your NAS?
- does the phonehome service fail when phoning home?
  - check the log
  - try manually ssh-ing to your NAS, the keypair permissions have failed me much. [This conversation](https://superuser.com/questions/215504/permissions-on-private-key-in-ssh-folder) seems to answer that.
  - try to run a second sshd on your NAS with extra verbosity and connect to that, and see what the logs say:
    - on NAS:
    ```sh
    $(which sshd) -ddd -p 1234
    ```
    - on offsite-server:
    ```sh
    ssh <NAS-SSH-USER>@hem -p <FW-PORT> -vvvvv
    ```
  - check on your NAS that the connection is open
    ```sh
    netstat -lntu
    ```
    you should see
    ```sh
    tcp        0      0 127.0.0.1:<LOCAL-PORT>         0.0.0.0:*               LISTEN
    ```
    if so, you should be able to ssh back to the offsite-server via the ./connectBackupOffsite.sh
- can you connect to the ftp without the mumbo jumbo? can you ftp from your PC? can you ftp from the offsite-server on LAN, without socks proxy? The FTP-server needs to support passive mode, make sure your NAS does

- If you have read this far and are not me and you still have problems, well I'm quite sure I've missed something but I'm not going to disassemble my running server to make sure, so You'll have to create an issue in this repo and maybe we can figure out where I missed out.

# TODO

- [x] list files
- [x] make the ftp transfer
- [x] make an initial file transfer
- [ ] schedule get_file.sh
- [ ] Make the get_file.sh accept a credential file instead of hard coding username and password (which feels really icky)
